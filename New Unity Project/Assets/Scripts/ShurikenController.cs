﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShurikenController : MonoBehaviour {

    public Vector2 speed;
	private Rigidbody2D rb;
	private int damage = 20;
	GameObject enemy;
	GameObject player;

	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody2D> ();
		rb.velocity = speed;

		enemy = GameObject.FindGameObjectWithTag ("Enemy");
		player = GameObject.FindGameObjectWithTag ("Player");
		
	}
	
	// Update is called once per frame
	void Update () {
		rb.velocity = speed;
	}

	private void OnTriggerEnter2D(Collider2D other){
		if (other.gameObject.CompareTag ("Enemy")) {
			other.SendMessageUpwards ("DamageEnemy", damage);
			Destroy(gameObject); 				
		}





	}


}
