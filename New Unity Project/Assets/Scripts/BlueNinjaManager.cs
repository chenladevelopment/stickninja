﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlueNinjaManager : MonoBehaviour {

	private Animator animator;
	private int maxHealth = 100;
	private int currentHealth;
	private float dieAnimationDuration = 0.2f;
	private float attackRange = 1f;
	private int damage = 20;
	private float lastAttackTime;
	private float attackDelay = 2f;

	private bool facingRight  = false;

	private GameObject player;

	// Use this for initialization
	void Start () {
		currentHealth = maxHealth;
		animator = GetComponent<Animator>();	

		player = GameObject.FindGameObjectWithTag ("Player");
	}
	
	// Update is called once per frame
	void Update () {

		if (currentHealth <= 0 && gameObject != null) {
			animator.SetBool ("Die", true);
			Destroy (gameObject,  animator.GetCurrentAnimatorStateInfo(0).length + dieAnimationDuration); 
		}

		if (player != null) {
			Flip ();

			float distanceToPlayer = Vector3.Distance (transform.position, player.transform.position);
			if (distanceToPlayer < attackRange) {
				if (Time.time > lastAttackTime + attackDelay) {
					animator.SetTrigger ("Attacking");
					StartCoroutine(CalculateDamageAfterDelay(0.5f));

					lastAttackTime = Time.time;
				}

			} 
		}
	}

	private void Flip() {

		if (transform.position.x < player.transform.position.x  && !facingRight || transform.position.x > player.transform.position.x && facingRight) {
			facingRight = !facingRight;

			Vector3 temp = transform.localScale;
			temp.x *= -1;
			transform.localScale = temp;
		}
	}

	IEnumerator CalculateDamageAfterDelay(float delayTime){
		yield return new WaitForSeconds(delayTime);
		player.transform.SendMessageUpwards ("Damage", damage);
	}
		

	public void DamageEnemy(int dmg){
		
		currentHealth -= dmg;
		animator.SetTrigger("Hurt");
	}
}
