﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeAttack1Controller : MonoBehaviour {

	private int dmg = 50;
	private float delayTimer = 0.2f;

	void OnTriggerEnter2D(Collider2D col){
		if (col.CompareTag ("Enemy")) {
			StartCoroutine(CalculateDamageAfterDelay(col, delayTimer));

		}
	}

	IEnumerator CalculateDamageAfterDelay(Collider2D col, float delayTime){
		yield return new WaitForSeconds(delayTime);
		col.SendMessageUpwards ("DamageEnemy", dmg);
	}
}
