﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangeNinjaController : MonoBehaviour {

	public GameObject leftShuriken;
	public GameObject rightShuriken;

	private Transform throwPosition;

	private bool facingRight  = false;

	private Animator animator;
	private int maxHealth = 100;
	private int currentHealth;
	private float dieAnimationDuration = 0.2f;
	private float attackRange = 50f;
	private int damage = 20;
	private float lastAttackTime;
	private float attackDelay = 2f;

	private GameObject player;

	// Use this for initialization
	void Start () {
		currentHealth = maxHealth;
		animator = GetComponent<Animator>();	
		throwPosition = transform.Find ("throwPosition");
		player = GameObject.FindGameObjectWithTag ("Player");
	}
	
	// Update is called once per frame
	void Update () {
		if (currentHealth <= 0 && gameObject != null) {
			animator.SetBool ("Die", true);
			Destroy (gameObject,  animator.GetCurrentAnimatorStateInfo(0).length + dieAnimationDuration); 
		}

		if (player != null) {
			Flip ();
			float distanceToPlayer = Vector3.Distance (transform.position, player.transform.position);
			if (distanceToPlayer < attackRange) {
				if (Time.time > lastAttackTime + attackDelay) {

					ThrowShuriken();

					lastAttackTime = Time.time;
				}

			} 
		}



	}

	private void Flip() {

		if (transform.position.x < player.transform.position.x  && !facingRight || transform.position.x > player.transform.position.x && facingRight) {
			facingRight = !facingRight;

			Vector3 temp = transform.localScale;
			temp.x *= -1;
			transform.localScale = temp;
		}
	}

	private void checkDirection(){
		if (transform.position.x < player.transform.position.x) {
			facingRight = false;
		} else {
			facingRight = true;
		}
	}

	public void ThrowShuriken(){
		if (facingRight) {
			Instantiate (rightShuriken, throwPosition.position, Quaternion.identity);
		} else {
			Instantiate (leftShuriken, throwPosition.position, Quaternion.identity);
		}

	}

	public void DamageEnemy(int dmg){

		currentHealth -= dmg;
		animator.SetTrigger("Hurt");
	}
}
