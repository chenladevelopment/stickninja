﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour {
    public float speedX;
    public float jumpSpeedY;
    public float delayBeforeDoubleJump;
	private float dieAnimationDuration = 0.2f;
	private bool attacking1 = false;
	private float attack1Cd = 0.5f;
	private float attack1Timer = 0.5f;

	private bool attacking5 = false;
	private float attack5Cd = 0.5f;
	private float attack5Timer = 0.5f;

	private int currentHealth = 100;
	private int maxHealth = 100;

	public Collider2D meleeAttackPosition;

	public GameObject leftShuriken;
	public GameObject rightShuriken;
	public GameObject superPower;

	private Transform throwPosition;
	private Transform superPowerPosition;

	private bool facingRight;
	private bool jumping;
	private bool running = false;
	private bool isGrounded;
	private bool canDoubleJump;
	private float speed;

	private Animator animator;
	private Animation animation;
	private Rigidbody2D rb;


	// Use this for initialization
	void Start () {
        animator = GetComponent<Animator>();
		animation = GetComponent<Animation> ();
        rb = GetComponent<Rigidbody2D>();

        facingRight = true;
        isGrounded = true;

		throwPosition = transform.Find ("throwPosition");
		superPowerPosition = transform.Find ("superPowerPosition");

		meleeAttackPosition.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
        MovePlayer(speed);
        Flip();
        if (Input.GetKeyDown(KeyCode.LeftArrow)) {
			MoveLeft ();
        }
        if (Input.GetKeyUp(KeyCode.LeftArrow)) {
			Idle ();
        }
        if (Input.GetKeyDown(KeyCode.RightArrow)){
			MoveRight ();
        }
        if (Input.GetKeyUp(KeyCode.RightArrow)){
			Idle ();
        }
        if (Input.GetKeyDown(KeyCode.UpArrow)){
            Jump();
        }
		if(Input.GetKeyDown(KeyCode.Space)){
			ThrowShuriken();
		}

		if (Input.GetKeyDown (KeyCode.Alpha1) ) {
			if (isGrounded) {
				attacking1 = true;
				meleeAttackPosition.enabled = true;
			}


		}
		if (Input.GetKeyDown (KeyCode.F)) {
			
				if (running) {
					Idle ();
					animator.SetInteger ("SuperPower", 1);
				} else {
					animator.SetInteger ("SuperPower", 1);
				}



		}
		if (Input.GetKeyUp (KeyCode.F)) {
			
				animator.SetInteger ("SuperPower", 2);
				StartCoroutine(ReleaseSuperPower ());


		
		}


		Attack1 ();
		if (currentHealth <= 0 && gameObject != null) {
			currentHealth = 0;
			animator.SetBool ("Die", true);
			Destroy (gameObject,  animator.GetCurrentAnimatorStateInfo(0).length + dieAnimationDuration); 


		}

    }



	private void Attack1(){
		if (attacking1) {
			if (attack1Timer > 0) {
				attack1Timer -= Time.deltaTime;
			} else {
				attacking1 = false;
				meleeAttackPosition.enabled = false;
				attack1Timer = attack1Cd;
			}
		}
			
		animator.SetBool ("Attacking1", attacking1);
	}

    private void MovePlayer(float playerSpeed){

		if (playerSpeed < 0 && !jumping || playerSpeed > 0 && !jumping) {
            animator.SetInteger("State", 11);
        }
		if (playerSpeed == 0 && !jumping) {
            animator.SetInteger("State", 0);
        }
        rb.velocity = new Vector3(playerSpeed, rb.velocity.y, 0);   
    }

    private void OnCollisionEnter2D(Collision2D collision){
		
        if (collision.gameObject.tag == "Ground") {
			animator.SetBool ("Jump", false);
			jumping = false;
			isGrounded = true;
			canDoubleJump = false;
			running = false;
        }
    }

    private void Flip() {
        if (speed > 0 && !facingRight || speed < 0 && facingRight) {
            facingRight = !facingRight;

            Vector3 temp = transform.localScale;
            temp.x *= -1;
            transform.localScale = temp;
        }
    }

    private void EnableDoubleJump() {
        canDoubleJump = true;
    }




    public void MoveLeft() {
		if (!attacking1) {
			running = true;
			speed = -speedX;
		}
    }
    public void MoveRight() {
		if (!attacking1) {
			running = true;
			speed = speedX;
		}
    }
    public void Idle() {
		running = false;
        speed = 0;
    }
    public void Jump() {
        if (isGrounded) {
            isGrounded = false;
            jumping = true;
			running = false;
            rb.AddForce(new Vector2(rb.velocity.x, jumpSpeedY));
            animator.SetBool("Jump", true);
            Invoke("EnableDoubleJump", delayBeforeDoubleJump);
        }
        if (canDoubleJump) {
            canDoubleJump = false;
            rb.AddForce(new Vector2(rb.velocity.x, jumpSpeedY));
			animator.SetBool("Jump", true);
        }
    }

	public void ThrowShuriken(){
		if (facingRight) {
			Instantiate (rightShuriken, throwPosition.position, Quaternion.identity);
		} else {
			Instantiate (leftShuriken, throwPosition.position, Quaternion.identity);
		}

	}

	public IEnumerator ReleaseSuperPower(){
		yield return new WaitForSeconds(0.1f);
		Instantiate (superPower, superPowerPosition.position, Quaternion.identity);
	}

	public void Damage(int damage){
		currentHealth -= damage;
		animator.SetTrigger ("Hurt");
	}


}
