﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SuperPowerController : MonoBehaviour {

	private int damage = 150;

	// Use this for initialization

	private void OnTriggerEnter2D(Collider2D col){
		if (col.CompareTag ("Enemy")) {
			col.SendMessageUpwards ("DamageEnemy", damage);
		}

	}
}
